@ECHO OFF
for %%f in (.\*) do (
    if not %%~xf==.bat ( 
        set /p val=<%%f
        echo fullname: %%f
        echo name: %%~nf
        echo extension: %%~xf
        echo contents: !val!
    )
)
